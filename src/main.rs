#![feature(try_blocks)]

use clap::{AppSettings, Clap, ValueHint};
use id3::Tag;
use std::collections::HashMap;

#[derive(Clap, Debug)]
#[clap(global_setting = AppSettings::ColoredHelp)]
struct Opts {
    #[clap(long)]
    collect_frame_types: bool,

    #[clap(value_hint = ValueHint::FilePath)]
    files: Vec<String>,
}

fn main() -> Result<(), id3::Error> {
    let opts: Opts = Opts::parse();

    let mut frame_types = if opts.collect_frame_types {
        Some(HashMap::<String, u32>::new())
    } else {
        None
    };

    for file in opts.files {
        println!("{}", file);
        let res: Result<(), id3::Error> = try {
            let tag = Tag::read_from_path(file)?;
            for frame in tag.frames() {
                print!(
                    "  {:60} ({}): {}",
                    frame.name(),
                    frame.id(),
                    frame.content()
                );
                if let Some(data) = frame.content().unknown() {
                    if data.len() <= 64 {
                        if let Ok(s) = std::str::from_utf8(data) {
                            print!(": {:?}", s)
                        }
                    }
                }
                println!();

                if let Some(frame_types) = &mut frame_types {
                    *frame_types
                        .entry(format!("{:60} ({})", frame.name(), frame.id()))
                        .or_default() += 1;
                    if let Some(text) = frame.content().extended_text() {
                        *frame_types
                            .entry(format!("{:60} ({})", text.description, frame.id()))
                            .or_default() += 1;
                    }
                    if let Some(link) = frame.content().extended_link() {
                        *frame_types
                            .entry(format!("{:60} ({})", link.description, frame.id()))
                            .or_default() += 1;
                    }
                }
            }
        };
        if let Err(e) = res {
            println!("  {:60}: {}", "Error", e)
        }
        println!()
    }

    if let Some(frame_types) = frame_types {
        println!("Encountered Frame Types:");
        let mut frame_types: Vec<_> = frame_types.iter().collect();
        frame_types.sort_by(|a, b| b.1.cmp(a.1));
        for frame_type in frame_types.iter() {
            println!("  {}: {}", frame_type.0, frame_type.1)
        }
    }

    Ok(())
}
