# id3-dump
Dump [ID3 Tags](https://en.wikipedia.org/wiki/ID3) which are commonly used for metadata in MP3 files.

Made possible by the [`id3` Rust Crate](https://docs.rs/id3).

## Installation
Build using [cargo](https://www.rust-lang.org/tools/install):

```sh
git clone https://gitlab.com/Ma_124/id3-dump && cd id3-dump
cargo build --locked --release
target/release/id3-dump --help
```

Or download a prebuilt binary:
- [Linux   (GNU/x64)](https://gitlab.com/Ma_124/id3-dump/-/jobs/artifacts/master/raw/bin/id3-dump?job=build)
- [Windows (GNU/x64)](https://gitlab.com/Ma_124/id3-dump/-/jobs/artifacts/master/raw/bin/id3-dump.exe?job=build)

## Usage

```
➜ id3-dump ~/Music/MDK/Astral\ Badass/05\ Altitude.mp3
/home/ma/Music/MDK/Astral Badass/05 Altitude.mp3
  Title/songname/content description                           (TIT2): Altitude
  Lead performer(s)/Soloist(s)                                 (TPE1): MDK
  Track number/Position in set                                 (TRCK): 5/12
  Album/Movie/Show title                                       (TALB): Astral Badass
  Part of a set                                                (TPOS): 1/1
  Recording time                                               (TDRC): 2017-12-02
  BPM (beats per minute)                                       (TBPM): 0
  TCMP                                                         (TCMP): 0
  Language(s)                                                  (TLAN): eng
  Band/orchestra/accompaniment                                 (TPE2): MDK
  Performer sort order                                         (TSOP): MDK
  Unsynchronised lyric/text transcription                      (USLT):
  Involved people list                                         (TIPL): arranger
  Original release time                                        (TDOR): 2017-12-02
  User defined text information frame                          (TXXX): Script: Latn
  Media type                                                   (TMED): Digital Media
  User defined text information frame                          (TXXX): Artist Credit: MDK
  User defined text information frame                          (TXXX): R128_ALBUM_GAIN: 0
  User defined text information frame                          (TXXX): R128_TRACK_GAIN: 0
  User defined text information frame                          (TXXX): ALBUMARTISTSORT: MDK
  User defined text information frame                          (TXXX): Album Artist Credit: MDK
  User defined text information frame                          (TXXX): MusicBrainz Album Type: album
  User defined text information frame                          (TXXX): MusicBrainz Album Status: Official
  User defined text information frame                          (TXXX): MusicBrainz Album Release Country: XW
  Comments                                                     (COMM): Visit http://mdkofficial.bandcamp.com
  User defined text information frame                          (TXXX): MusicBrainz Album Id: 4de036e1-4ea3-4451-8431-ba276e932624
  Unique file identifier                                       (UFID): Unknown, 59 bytes: "http://musicbrainz.org\u{0}2a219595-db22-4ecd-acb3-1e01832ae6a9"
  User defined text information frame                          (TXXX): MusicBrainz Artist Id: 12bd9bf5-72cf-496d-869a-af32ea53da49
  User defined text information frame                          (TXXX): MusicBrainz Album Artist Id: 12bd9bf5-72cf-496d-869a-af32ea53da49
  User defined text information frame                          (TXXX): MusicBrainz Release Group Id: d1d6c88d-6bcc-4afa-83c8-4b0138cc71c5
  User defined text information frame                          (TXXX): MusicBrainz Release Track Id: bbd2689f-cbf8-4ec3-80dd-268d8c6754d7
  Attached picture                                             (APIC): cover: Front cover (image/jpeg, 204382 bytes)
```

## License
Copyright &copy; 2021 Ma_124
[License](./LICENSE)
